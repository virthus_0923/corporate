#/bin/bash

function git_diff_archive()
{
  local diff=""
  local h="HEAD"
  if [ $# -eq 1 ]; then
    if expr "$1" : '[0-9]*$' > /dev/null ; then
      diff="HEAD~${1} HEAD"
    else
      diff="${1} HEAD"
    fi
  elif [ $# -eq 2 ]; then
    diff="${2} ${1}"
    h=$1
  fi
  if [ "$diff" != "" ]; then
    diff="git diff --diff-filter=d --name-only ${diff}"
  fi

  local date=`date +%Y%m%d%H%M`
  git archive --format=zip --prefix=source_$date/ $h `eval $diff` -o soruce_$date.zip
}

cd `dirname $0`/../
git_diff_archive ${1} ${2}
