/**
* common
*/

/*globals $ */
(function () {
    'use strict';

    // =======================================
    // smooth scroll
    // =======================================
    $(function () {
        var $gaps = $('[data-scroll-gap]');
        $(document).on('click', '[href^=\\#]', function () {
            var $offsetElem = $(this),
            $parentElem = $(this).closest('[data-scroll-offset]');
            if (!$(this).is('[data-scroll-offset]') && $parentElem.length)
            $offsetElem = $parentElem;
            var scrollTo = 0,
            offsetTop = parseInt($offsetElem.attr('data-scroll-offset') || 0, 10) * -1,
            target = $(this).attr('data-scroll') || $(this).attr('href') || '#pageTop';

            if (target === '#') {
                return false;
            } else if (target === '#pageTop') {
                scrollTo = 0;
            } else {
                scrollTo = $(target).offset().top;
                $gaps.filter(':visible').each(function () {
                    scrollTo -= $(this).outerHeight();
                });
                scrollTo += offsetTop;
            }

            $('html,body').stop().animate({scrollTop: Math.max(0, scrollTo)}, 1300, 'easeInOutQuad');
            return false;
        });
    });

})();

$(function(){
    $('#nav_toggle, #nav_menu nav a').click(function(e){
        if(!matchMedia('(max-width: 767px)').matches)return;
        $("#nav_menu").toggleClass('open');
        $("nav").slideToggle(500);
    });
});


$(function() {
    $('.slider').slick({
        infinite: true,
        dots:true,
        slidesToShow: 1,
        centerMode: true, //要素を中央寄せ
        autoplay:true, //自動再生
        variableWidth: true,
        appendArrows: $('.arrows'),
        prevArrow: '<div class="slider-arrow slider-prev"><img src="images/top/slide_arrow-l.png" alt="＜"></div>',
        nextArrow: '<div class="slider-arrow slider-next"><img src="images/top/slide_arrow-r.png" alt="＞"></div>',
        responsive: [{
               breakpoint: 768,
                    settings: {
                         centerMode: false,
                         variableWidth: false,
               }
          }]
    });
});
